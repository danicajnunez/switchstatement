package com.mycompany.switchstatement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author nunezd
 */
public class SwitchWithInts {

    public static void main(String[] args) {
        String input = getInput("Enter a number between 1 and 12: ");
        int month = Integer.parseInt(input);

        switch (month) {
            case 1:
                System.out.println("The month is January");
                break; //jump to the end of the code block

            case 2:
                System.out.println("The month is February");
                break;

            case 3:
                System.out.println("The month is March");
                break;
            default:
                System.out.println("You chose another month");
                break;
        }
    }

    private static String getInput(String prompt) {
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));

        System.out.print(prompt);
        System.out.flush();

        try {
            return stdin.readLine();
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }
}
