package com.mycompany.switchstatement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author nunezd
 */
public class SwitchWithEnums {
    
    //The advantage of enumeration is that you are limiting the possibilities
    //to only these values. When you say something is an integer you're not 
    //limiting yourself to 1 to 3, you have millions of possibilities. 
    
    public static void main(String[] args) {
        Month month = Month.FEBRUARY;
        
        
        switch(month){
            case JANUARY:
                System.out.println("It's the first month");
                break;
            
            case FEBRUARY:
                System.out.println("It's the second month");
                break;
            
            case MARCH:
                System.out.println("It's the third month");
        }
    }
    
}
